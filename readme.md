# Loket Stream SDK
## Installation
```
add composer.json
"repositories": [
        {
            "type": "vcs",
            "url": "https://source.golabs.io/loket/self-service-platform/loket-stream-sdk-php.git",
            "name": "loket/loket-stream-sdk-php"
        }
    ]
"require": {
    ....,
    "loket/loket-stream-sdk-php": "0.1.*",
},
```
```
add value to .env
LOKET_STREAM_HOST=
```