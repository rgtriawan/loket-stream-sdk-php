<?php

namespace Loket\LoketStreamSDK\Client\LoketStream;

class LoketStream
{
    use LoketStreamClient;
    private static $instance = null;

    private function __constructor()
    { }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public static function detailStream(array $parameters, string $reference_id)
    {
        return self::getInstance()->createRequest("GET", "/api/v1/stream/info/$reference_id", $parameters);
    }

    public static function createStream(array $parameters, string $reference_id)
    {
        return self::getInstance()->createRequest("POST", "/api/v1/stream/create/$reference_id", $parameters);
    }

    public static function startStream(array $parameters, string $reference_id)
    {
        return self::getInstance()->createRequest("PUT", "/api/v1/stream/start/$reference_id", $parameters);
    }

    public static function stopStream(array $parameters, string $reference_id)
    {
        return self::getInstance()->createRequest("PUT", "/api/v1/stream/stop/$reference_id", $parameters);
    }
}
