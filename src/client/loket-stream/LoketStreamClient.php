<?php

namespace Loket\LoketStreamSDK\Client\LoketStream;

use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;

trait LoketStreamClient
{

    private function getContent($response)
    {
        $content = $response->getBody()->getContents();
        return @json_decode($content, 1) ? @json_decode($content, 1) : $content;
    }

    public function createRequest($method, $url, $parameters = [], $noCache = false)
    {

        $httpClient    = new \GuzzleHttp\Client();
        $fullPath = getenv("LOKET_STREAM_HOST") . $url;
        try {
            if ($method == "POST" || $method == "PUT") {
                $response = $httpClient->request($method, $fullPath, [
                    "form_params" => $parameters,
                    'headers'     => [
                        "Accept" => "application/json",
                    ]
                ]);
            } else {
                $response = $httpClient->request($method, $fullPath, [
                    "query"   => $parameters,
                    'headers' => [
                        "Accept" => "application/json",
                    ]
                ]);
            }
            return $this->getContent($response);
        } catch (BadResponseException $exception) {
            $request  = $exception->getRequest();
            $response = $exception->getResponse();
            $uri      = $request->getUri();
            $content  = $this->getContent($response);
            $code     = $exception->getCode();
            return $content;
        } catch (RequestException $exception) {
            $request  = $exception->getRequest();
            $response = $exception->getResponse();
            $uri      = $request->getUri();
            $content  = $this->getContent($response);
            $code     = $exception->getCode();
            return $content;
        }
    }
}
